#define _DEFAULT_SOURCE

#include <stdio.h>

#include "mem.h"
#include "mem_internals.h"
#include "tests.h"

enum {
    HEAP_SIZE = 8192,
    BLOCK_CAP = 1024
};

static void print_success(int n) {
    printf("\nТест №%d успешно пройден\n", n);
}

static void print_error(int n) {
    printf("\nОшибка при выполнении теста №%d\n", n);
}

static void print_separator() {
    printf("------------------------------------------------------------------");
}

static size_t capacity(size_t size) {
    return capacity_from_size((block_size) {size}).bytes;
}

static void free_heap(void *heap, size_t size) {
    munmap(heap, size_from_capacity((block_capacity) {size}).bytes);
}

static struct block_header *block_get_header(void *contents) {
    return (struct block_header *) (((uint8_t *) contents) - offsetof(struct block_header, contents));
}

static void test1() {
    print_separator();
    printf("\nТест №1: обычное успешное выделение памяти.\n\n");

    void *heap = heap_init(capacity(HEAP_SIZE));
    debug_heap(stdout, heap);

    void *allocated = _malloc(BLOCK_CAP);

    if (allocated) {
        debug_heap(stdout, heap);
        _free(allocated);
        debug_heap(stdout, heap);
        free_heap(heap, HEAP_SIZE);
        print_success(1);
    } else {
        print_error(1);
    }

    print_separator();
}

static void test2() {
    print_separator();
    printf("\nТест №2: освобождение одного блока из нескольких выделенных.\n\n");

    void *heap = heap_init(capacity(HEAP_SIZE));
    debug_heap(stdout, heap);

    void *allocated_1 = _malloc(BLOCK_CAP);
    void *allocated_2 = _malloc(BLOCK_CAP);
    void *allocated_3 = _malloc(BLOCK_CAP);
    bool all_allocated = allocated_1 && allocated_2 && allocated_3;

    if (all_allocated) {
        debug_heap(stdout, heap);
        _free(allocated_1);
        all_allocated = allocated_2 && allocated_3;
        if (all_allocated) {
            debug_heap(stdout, heap);
            _free(allocated_2);
            _free(allocated_3);
            debug_heap(stdout, heap);
            free_heap(heap, HEAP_SIZE);
            print_success(2);
        } else {
            print_error(2);
        }
    } else {
        print_error(2);
    }

    print_separator();
}

static void test3() {
    print_separator();
    printf("\nТест №3: освобождение двух блоков из нескольких выделенных.\n\n");

    void *heap = heap_init(capacity(HEAP_SIZE));
    debug_heap(stdout, heap);

    void *allocated_1 = _malloc(BLOCK_CAP);
    void *allocated_2 = _malloc(BLOCK_CAP);
    void *allocated_3 = _malloc(BLOCK_CAP);
    bool all_allocated = allocated_1 && allocated_2 && allocated_3;

    if (all_allocated) {
        debug_heap(stdout, heap);
        _free(allocated_1);
        _free(allocated_3);
        all_allocated = allocated_2;
        if (all_allocated) {
            debug_heap(stdout, heap);
            _free(allocated_2);
            debug_heap(stdout, heap);
            free_heap(heap, HEAP_SIZE);
            print_success(3);
        } else {
            print_error(3);
        }
    } else {
        print_error(3);
    }

    print_separator();
}

static void test4() {
    print_separator();
    printf("\nТест №4: расширение региона памяти.\n\n");

    void *heap = heap_init(capacity(HEAP_SIZE));
    debug_heap(stdout, heap);

    void *allocated = _malloc(HEAP_SIZE * 2);

    if (allocated) {
        debug_heap(stdout, heap);
        struct block_header *header = heap;
        if (header->capacity.bytes > HEAP_SIZE) {
            _free(allocated);
            debug_heap(stdout, heap);
            free_heap(heap, HEAP_SIZE);
            print_success(4);
        } else {
            print_error(4);
        }
    } else {
        print_error(4);
    }

    print_separator();
}

static void test5() {
    print_separator();
    printf("\nТест №5: память закончилась, старый регион памяти не расширить из-за другого выделенного диапазона адресов, новый регион выделяется в другом месте. \n\n");

    void *heap = heap_init(capacity(HEAP_SIZE));
    debug_heap(stdout, heap);

    (void) mmap(heap + HEAP_SIZE, HEAP_SIZE, PROT_READ | PROT_WRITE, MAP_PRIVATE | MAP_ANONYMOUS | MAP_FIXED, -1, 0);

    void *allocated = _malloc(10000);

    if (allocated) {
        debug_heap(stdout, heap);
        print_success(5);
    } else {
        print_error(5);
    }

    print_separator();
}

void run_tests() {
    test1();
    test2();
    test3();
    test4();
    test5();
}
